package web.controller;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.validation.Valid;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.RestTemplate;

import com.sun.research.ws.wadl.Response;

import lombok.extern.slf4j.Slf4j;
import web.model.Access;
import web.model.Company;
import web.model.EmployeeCompany;
import web.model.Payment;
import web.model.Service;
import web.model.Service.Type;


@Slf4j
@Controller
@RequestMapping("/access")
public class AccessController {
	private RestTemplate rest = new RestTemplate();

	
	@GetMapping
	public String showAccesss(Model model) {
		List<Access> accesss = Arrays
				.asList(rest.getForObject("http://localhost:8082/access", Access[].class));
		model.addAttribute("accesss",accesss);
		return "access"; 
	}
	@GetMapping("/create")
	public String createAccess(Model model) {
		Access access=new Access();
		model.addAttribute("access",access);
		List<EmployeeCompany> employeeCompanys = Arrays
				.asList(rest.getForObject("http://localhost:8082/employeeCompany", EmployeeCompany[].class));
		model.addAttribute("employeeCompanys",employeeCompanys);
		model.addAttribute("types",Access.Type.values());
		model.addAttribute("locations",Access.Location.values());
		return "createAccess"; 
	}
	
	@PostMapping("/create")
	//@ResponseBody
	public String processDesign(
			@RequestParam("Time") String Time, @RequestParam("location") String location,
			@RequestParam("accessCard") String accessCard, @RequestParam("type") String type) {
		LocalTime localTime = LocalTime.parse( Time );
		Date date = java.sql.Timestamp.valueOf(localTime.atDate(LocalDate.now()));
		List<EmployeeCompany> employeeCompanys = Arrays
				.asList(rest.getForObject("http://localhost:8082/employeeCompany", EmployeeCompany[].class));

		EmployeeCompany employeeCompany = rest.getForObject("http://localhost:8082/employeeCompany/"+accessCard, EmployeeCompany.class);
		//return employeeCompany.name;
		Access access=new Access(); 
		//access.setId(id);
		access.setEmployeeCompany(employeeCompany);
		for(Access.Location lc : Access.Location.values()) {
			if(lc.toString().equals(location)) {
				access.setLocation(lc);
			}
		}
		for(Access.Type tp : Access.Type.values()) {
			if(tp.toString().equals(type)) {
				access.setType(tp);
			}
		}
		access.setTime(date);
		access.setEmployeeCompany(employeeCompany);
		System.out.println(access);
		rest.postForObject("http://localhost:8082/access", access, Access.class);
//		rest.postForObject("http://localhost:8082/access", access, Access.class);
		return "redirect:/access";
	}
	@GetMapping("/delete/{id}")
	public String deleteEmployeeCompany(Model model, @PathVariable ("id") String id) {
		rest.delete("http://127.0.0.1:8082/access/delete/{id}", id);
		return "redirect:/access";
	}

}