package web.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.websocket.server.PathParam;

import org.apache.http.HttpStatus;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import lombok.extern.slf4j.Slf4j;
import web.model.Company;
import web.model.EmployeeCompany;
import web.model.Service;

@Slf4j
@Controller
@RequestMapping("/employeeCompany")
public class EmployeeCompanyController {
	private RestTemplate rest = new RestTemplate();

	
	@InitBinder
	public void initBinder(WebDataBinder webDataBinder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
	    dateFormat.setLenient(false);
	    webDataBinder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
	@GetMapping
	public String showEmployees(Model model) {
		List<EmployeeCompany> employees = Arrays
				.asList(rest.getForObject("http://127.0.0.1:8082/employeeCompany", EmployeeCompany[].class));
		model.addAttribute("employees", employees);
		return "employeeCompany";
	}

	@GetMapping("/create")
	public String createemployee(Model model) {
		EmployeeCompany employee = new EmployeeCompany();
		model.addAttribute("employee", employee);
		return "createEmployeeCompany";
	}

	@PostMapping("/create")
	//@ResponseBody
	public String processDesign(@RequestParam("accessCard") String accessCard, @RequestParam("CMT") String CMT,
			@RequestParam("name") String name, @RequestParam("dateOfBirth") String dateOfBirth,
			@RequestParam("phoneNumber") String phoneNumber, Model model,
			RedirectAttributes redirectAttributes) throws ParseException {
		EmployeeCompany employee = new EmployeeCompany(accessCard, CMT, name, new SimpleDateFormat("yyyy-MM-dd").parse(dateOfBirth)  , phoneNumber, null);
		try {
			employee = rest.postForObject("http://localhost:8082/employeeCompany", employee, EmployeeCompany.class);
		}catch(Exception se) {
			model.addAttribute("erro", employee.name);
			//model.addAttribute("erro", "Thêm thất bại, thử lại!");
			model.addAttribute("employee", employee);
			return "createEmployeeCompany";
		}
		redirectAttributes.addAttribute("success", employee.accessCard);
		return "redirect:/employeeCompany";
	}

	@GetMapping("/update/{accessCard}")
	//@ResponseBody
	public String updateEmployee(Model model, @PathVariable ("accessCard") String accessCard) {
		EmployeeCompany employeeCompany = rest.getForObject("http://localhost:8082/employeeCompany/"+accessCard, EmployeeCompany.class);
		model.addAttribute("employee", employeeCompany);
		//return accessCard;
		//return employeeCompany.name;
		return "updateEmployeeCompany";
	}

	@PutMapping("/update/{accessCard}")
	public String process(@RequestParam("CMT") String CMT, @PathVariable ("accessCard") String accessCard,
			@RequestParam("name") String name, @RequestParam("dateOfBirth") String dateOfBirth,
			@RequestParam("phoneNumber") String phoneNumber) throws ParseException {
		EmployeeCompany employee = new EmployeeCompany(accessCard, CMT, name, new SimpleDateFormat("yyyy-MM-dd").parse(dateOfBirth), phoneNumber, null);
		rest.put("http://localhost:8082/employeeCompany/{accessCard}", employee, employee.getAccessCard());
		return "redirect:/employeeCompany";
	}

	@GetMapping("/delete/{accessCard}")
	public String deleteEmployeeCompany(Model model, @PathVariable ("accessCard") String accessCard) {
		rest.delete("http://127.0.0.1:8082/employeeCompany/{accessCard}", accessCard);
		return "redirect:/employeeCompany";
	}

	@DeleteMapping("/delete")
	public String processService(@RequestParam("CMT") String CMT) {
		rest.delete("http://127.0.0.1:8082/employeeCompany/{CMT}", CMT);
		return "redirect:/employeeCompany";
	}
}
