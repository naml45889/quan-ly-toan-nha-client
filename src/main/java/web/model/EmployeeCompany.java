package web.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
public class EmployeeCompany {
	public final String accessCard;
	public final String CMT;
	public final String name;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public final Date dateOfBirth;
	public final String phoneNumber; 

	public final List<Access> accesses; 
}
