package web.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
@Data
//@RequiredArgsConstructor
@NoArgsConstructor(access = AccessLevel.PUBLIC, force = true)
public class EmployeeBuilding {
	public int id;
	public String name;
	public Date dateOfBirth;
	public String address;
	public String phoneNumber;
	public int ranking;
	public String position;
	public List<Service> services;
	public double salary;
}	
